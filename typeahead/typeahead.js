(function() {

"use strict";


//start typing in the text box and typeahead predicitons should be shown.
//todo - detect right arrow key and move cursor to end of text field.

var ractive = new Ractive({
  el: '#container',
  // We could pass in a string, but for the sake of convenience
  // we're passing the ID of the <script> tag above.
  template: '#template',

  // Here, we're passing in some initial data
  data: { 
          pageTitle: 'Find fruits !',
          searchKey: '', 
          hint: 'Type something ...'
        }
            
});
    

var db = [ 'Apple', 'Apricot', 'Avocado', 'Banana', 'Bilberry', 'Blackberry', 'Blackcurrant', 'Blueberry', 'Boysenberry', 
       'Cantaloupe', 'Currant', 'Cherry', 'Cherimoya', 'Cloudberry', 'Coconut', 'Cranberry', 'Damson', 'Date', 'Dragonfruit',
       'Durian', 'Elderberry', 'Feijoa', 'Fig', 'Gooseberry', 'Grape', 'Raisin', 'Guava', 'Jackfruit', 'Jambul', 'Kiwi',
       'Lemon', 'Lychee', 'Mango', 'Melon', 'Honeydew', 'Watermelon', 'Mulberry', 'Nectarine', 'Olive', 'Orange',
       'Papaya', 'Passionfruit', 'Peach', 'Pear', 'Plum', 'Pineapple', 'Pomegranate', 'Raspberry', 'Strawberry', 'Tamarind'];
 

//simple function to return matches    
var predict = function(input) {
        
  var found;
  if ( _.isString(input) && input.length > 0 ) 
  {
        found = _.filter(db, function(element) { 
        return _.startsWith(_.toLower(element), _.toLower(input), 0); 
        })[0];
  }
  console.log(found);
  if ( ! _.isUndefined(found) ) { 
    return _.toLower(found); 
  } else { 
    return '';
  } 
         
};    
    
//watch input field 
ractive.observe('searchKey', function(newValue, oldValue, keypath){
  var res = predict(ractive.get('searchKey'));
  console.log('got res as ' + res );
  ractive.set('predicted', res);
});
    
})();